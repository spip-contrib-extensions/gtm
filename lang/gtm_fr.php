<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/gis/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	'label_tag_gtm'         => 'Code du tag',
	'explication_tag_gtm'   => 'Insérez ici le code complet du Google Tag Manager tel qu\'il vous a été fourni sur votr page Google Analytics',
	'configurer_titre'      => 'Google Tag Manager',
	'configurer_descriptif' => 'Configurer le Google Tag Manager pour votre site. Vous pouvez visiter ici une <a href="https://www.google.com/intl/fr/tagmanager/"> page de présentation du Google Tag Manager</a> (ou Gestionnaire de Balise Google).',
	'placeholder'           => '<!-- Google Tag Manager -->',
	'erreur_ga_actif'       => 'Attention, le plugin Google Analytics est actif, veuillez le désactiver avant de pouvoir utiliser GTM.',
);

?>
